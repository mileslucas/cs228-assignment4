package edu.iastate.cs228.hw4;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Extension of the AbstractCollection class based on a Binary Search Tree.
 * Efficiencies may vary by implementation, but all methods should have at least
 * the worst case runtimes of a standard Tree.
 * 
 * @author
 */
public class BinarySearchTree<E extends Comparable<? super E>> extends AbstractCollection<E> {

    /**
     * Member variables to support the tree: - A Node referencing the root of
     * the tree - An int specifying the element count
     */
    private Node<E> root;
    private int size;

    /**
     * Constructs an empty BinarySearchTree
     */
    public BinarySearchTree() {
	this(null, 0);
    }

    /**
     * Constructs a new BinarySearchTree whose root is exactly the given Node.
     * (For testing purposes, set the root to the given Node, do not clone it)
     * 
     * @param root
     *            - The root of the new tree
     * @param size
     *            - The number of elements already contained in the new tree
     */
    public BinarySearchTree(Node<E> root, int size) {
	this.root = root;
	this.size = size;
    }

    /**
     * Adds the given item to the tree if it is not already there.
     * 
     * @return false if item already exists in the tree and true otherwise.
     * @param item
     *            - Item to be added to the tree
     * @throws IllegalArgumentException
     *             - If item is null
     */
    @Override
    public boolean add(E item) throws IllegalArgumentException {
	if (item == null) {
	    throw new IllegalArgumentException("Cannot add a null item");
	}
	// Check if the tree already contains the given item
	if (contains(item)) {
	    return false;
	}
	insert(root, item);
	// Increment the size
	size++;
	return true;
    }

    /**
     * Recursively adds an item into the tree given a start root
     * 
     * @param currentRoot
     *            The root to start insert from
     * @param item
     *            the data of the item to insert
     */
    private void insert(Node<E> currentRoot, E item) {
	Node<E> n = new Node<E>(item);
	// If the tree is empty then we just make a new root Node
	if (root == null) {
	    root = n;
	    return;
	}
	// Splay the tree to the current item and root
	root = splay(root, item);
	int comp = item.compareTo(root.getData());
	// If the item belongs in the left child then zig the root to the right
	if (comp < 0) {
	    n.setLeft(root.getLeft());
	    n.setRight(root);
	    root.setLeft(null);
	} // Otherwise zig the root to the left
	else {
	    n.setRight(root.getRight());
	    n.setLeft(root);
	    root.setRight(null);
	}
	// Our new root is the inserted item's node
	root = n;
    }

    /**
     * Removes the given item from the tree if it is there. Because the item is
     * an Object it will need to be cast to an E type. To verify that this is a
     * safe cast, compare its class to the class of the root Node's data.
     * 
     * @return false if the list is empty or item does not exist in the tree,
     *         true otherwise
     * @param item
     *            - The item to be removed from the tree
     */
    @Override
    public boolean remove(Object item) {
	if (isEmpty() || !contains(item) || root.getData().getClass() != item.getClass()) {
	    return false;
	}
	// Should be safe due to our comparison of the two classes
	E key = (E) item;
	// Splay the tree to bring the root containing the data to the root.
	root = splay(root, key);
	// If the item has nothing less than it, just reassign the right child
	// to the root
	if (root.getLeft() == null) {
	    root = root.getRight();
	} // Otherwise bring the left of the root to the root, splay, and attach
	  // the right tree to the new root
	else {
	    Node<E> tempNode = root.getRight();
	    root = root.getLeft();
	    splay(root, key);
	    root.setRight(tempNode);
	}
	// Decrement the size
	size--;
	return true;
    }

    /**
     * Retrieves data of the Node in the tree that contains item. i.e. the data
     * such that Node.data.equals(item) is true
     * 
     * @return null if item does not exist in the tree, otherwise the data
     *         stored at the Node that meets the condition above.
     * @param item
     *            - The item to be retrieved
     */
    public E get(E item) {
	// Brings the node with the data to the root via splaying. This allows
	// the most used data to stay near the top of the tree
	root = splay(root, item);
	if (item.equals(root.getData())) {
	    return root.getData();
	} else {
	    return null;
	}

    }

    private Node<E> splay(Node<E> n, E item) {
	// Avoid Null pointer exception
	if (n == null) {
	    return null;
	}
	int comp = item.compareTo(n.getData());
	if (comp < 0) {
	    if (n.getLeft() == null) {
		return n;
	    }
	    int compLeft = item.compareTo(n.getLeft().getData());
	    if (compLeft < 0) {
		n.getLeft().setLeft(splay(n.getLeft().getLeft(), item));
		n = rotateRight(n);
	    } else if (compLeft > 0) {
		n.getLeft().setRight(splay(n.getLeft().getRight(), item));
		if (n.getLeft().getRight() != null) {
		    n.setLeft(rotateLeft(n.getLeft()));
		}
	    }
	    if (n.getLeft() == null) {
		return n;
	    } else {
		return rotateRight(n);
	    }
	} else if (comp > 0) {
	    if (n.getRight() == null) {
		return n;
	    }
	    int compRight = item.compareTo(n.getRight().getData());
	    if (compRight < 0) {
		n.getRight().setLeft(splay(n.getRight().getLeft(), item));
		if (n.getRight().getLeft() != null) {
		    n.setRight(rotateRight(n.getRight()));
		}
	    } else if (compRight > 0) {
		n.getRight().setRight(splay(n.getRight().getRight(), item));
		n = rotateLeft(n);
	    }

	    if (n.getRight() == null) {
		return n;
	    } else {
		return rotateLeft(n);
	    }
	} else {
	    return n;
	}
    }

    /**
     * Rotates the given node to the left child
     * 
     * @param n
     *            node to rotate around
     * @return The node that replaces the location of the given node
     */
    private Node<E> rotateLeft(Node<E> n) {
	Node<E> tempNode = n.getRight();
	n.setRight(tempNode.getLeft());
	tempNode.setLeft(n);
	return tempNode;
    }

    /**
     * Rotates the given node to the right child
     * 
     * @param n
     *            node to rotate around
     * @return The node that replaces the location of the given node
     */
    private Node<E> rotateRight(Node<E> n) {
	Node<E> tempNode = n.getLeft();
	n.setLeft(tempNode.getRight());
	tempNode.setRight(n);
	return tempNode;
    }

    /**
     * Tests whether or not item exists in the tree. i.e. this should only
     * return true if a Node exists in the tree such that Node.data.equals(item)
     * is true
     * 
     * @return false if item does not exist in the tree, otherwise true
     * @param item
     *            - The item check
     */
    @Override
    public boolean contains(Object item) {
	if (isEmpty() || item == null || root.getData().getClass() != item.getClass()) {
	    return false;
	}
	E data = (E) item;
	return get(data) != null;

    }

    /**
     * Removes all elements from the tree
     */
    @Override
    public void clear() {
	root = null;
	size = 0;
    }

    /**
     * Tests whether or not the tree contains any elements.
     * 
     * @return false if the tree contains at least one element, true otherwise.
     */
    @Override
    public boolean isEmpty() {
	return size == 0;
    }

    /**
     * Retrieves the number of elements in the tree.
     */
    @Override
    public int size() {
	return size;
    }

    /**
     * Returns a new BSTIterator instance.
     */
    @Override
    public Iterator<E> iterator() {
	return new BSTIterator();
    }

    /**
     * Returns an ArrayList containing all elements in the tree in the order
     * given by a preorder traversal of the tree.
     * 
     * @return an ArrayList of elements from the traversal.
     */
    public ArrayList<E> getPreorderTraversal() {
	ArrayList<E> list = new ArrayList<E>();
	preOrderTraversalRec(root, list);
	return list;
    }

    /**
     * Recursively travels through the tree in a pre-order path
     * 
     * @param current
     *            the current node that is being traversed
     * @param list
     *            the list holding the traveled nodes.
     */
    private void preOrderTraversalRec(Node<E> current, ArrayList<E> list) {
	if (current == null) {
	    return;
	}

	list.add(current.getData());
	preOrderTraversalRec(current.getLeft(), list);
	preOrderTraversalRec(current.getRight(), list);

    }

    /**
     * Returns an ArrayList containing all elements in the tree in the order
     * given by a postorder traversal of the tree.
     * 
     * @return an ArrayList of elements from the traversal.
     */
    public ArrayList<E> getPostorderTraversal() {
	ArrayList<E> list = new ArrayList<E>();
	postOrderTraversalRec(root, list);
	return list;
    }

    /**
     * Recursively travels through the tree in a post-order path
     * 
     * @param current
     *            the current node that is being traversed
     * @param list
     *            the list holding the traveled nodes.
     */
    private void postOrderTraversalRec(Node<E> current, ArrayList<E> list) {
	if (current == null) {
	    return;
	}
	postOrderTraversalRec(current.getLeft(), list);
	postOrderTraversalRec(current.getRight(), list);
	list.add(current.getData());

    }

    /**
     * Returns an ArrayList containing all elements in the tree in the order
     * given by a inorder traversal of the tree.
     * 
     * @return an ArrayList of elements from the traversal.
     */
    public ArrayList<E> getInorderTraversal() {
	ArrayList<E> list = new ArrayList<E>();
	inOrderTraversalRec(root, list);
	return list;
    }

    /**
     * Recursively travels through the tree in a in-order path
     * 
     * @param current
     *            the current node that is being traversed
     * @param list
     *            the list holding the traveled nodes.
     */
    private void inOrderTraversalRec(Node<E> current, ArrayList<E> list) {
	if (current == null) {
	    return;
	}
	inOrderTraversalRec(current.getLeft(), list);
	list.add(current.getData());
	inOrderTraversalRec(current.getRight(), list);

    }

    /**
     * Implementation of the Iterator interface which returns elements in the
     * order of an inorder traversal using Nodes predecessor and successor.
     * 
     * @author
     */
    private class BSTIterator implements Iterator<E> {
	private Node<E> next;
	private Node<E> last = null;

	public BSTIterator() {
	    next = root;
	    if (next != null) {
		// Iterate down to the minimum node (left-most)
		while (next.getLeft() != null) {
		    next = next.getLeft();
		}
	    }
	}

	/**
	 * Returns true if more elements exist in the inorder traversal, false
	 * otherwise.
	 */
	@Override
	public boolean hasNext() {
	    return next != null;
	}

	/**
	 * Returns the next item in the inorder traversal.
	 * 
	 * @return the next item in the traversal.
	 * @throws IllegalStateException
	 *             - if no more elements exist in the traversal.
	 */
	@Override
	public E next() throws IllegalStateException {
	    if (!hasNext()) {
		throw new IllegalStateException();
	    }
	    last = next;
	    next = next.getSuccessor();
	    return last.getData();

	}

	/**
	 * Removes the last item that was returned by calling next().
	 * 
	 * @throws IllegalStateException
	 *             - if next() has not been called yet or remove() is called
	 *             multiple times in a row.
	 */
	@Override
	public void remove() throws IllegalStateException {
	    if (last == null) {
		throw new IllegalStateException();
	    }
	    if (last.getLeft() != null && last.getRight() != null) {
		next = last;
	    }
	    BinarySearchTree.this.remove(last.getData());
	    last = null;
	}

    }
}
