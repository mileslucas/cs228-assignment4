package edu.iastate.cs228.hw4;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import edu.iastate.cs228.hw4.Dictionary.Entry;

/**
 * Some comprehensive tests for Dictionary and entry Please don't just rely on
 * these tests, because I may have missed some special cases Also, lemme know on
 * Blackboard if I missed any special cases, or if there are any issues with the
 * tests.
 * 
 * @author Peter Keppeler
 *
 */
public class DictionaryTests {
    // a bunch of instance variables
    private Entry word1;
    private Entry word2;

    private Entry word3;
    private Entry word4;

    private Entry word1d;
    private Entry word2d;
    private Entry word3d;
    private Entry word4d;
    private Entry word5d;
    private Entry word6d;
    private Entry word7d;

    private BinarySearchTree<Entry> tree;

    private Dictionary dictFromTree;

    // runs before every test
    @Before
    public void setup() throws FileNotFoundException {
	word1 = new Entry("animal");
	word2 = new Entry("animal", "a living thing");
	word3 = new Entry("cat");
	word4 = new Entry("cat", "demon pet");

	word1d = new Entry("map", "a paper");
	word2d = new Entry("hat", "a thing you wear");
	word3d = new Entry("rat", "a vermin");
	word4d = new Entry("arm");
	word5d = new Entry("kill");
	word6d = new Entry("orb", "a ball");
	word7d = new Entry("zork", "a noise");

	Entry[] temp = { word1d, word2d, word3d, word4d, word5d, word6d, word7d };

	tree = new BinarySearchTree<Entry>();

	for (int i = 0; i < temp.length; i++) {
	    tree.add(temp[i]);
	}

	dictFromTree = new Dictionary(tree);

	// ensures the file is reset to blank after the test
	File f1 = new File("toPrint1.txt");
	PrintWriter out = new PrintWriter(f1);
	out.close();
    }

    // compareTo for two identical words should be 0 even if only one has a
    // definition
    @Test
    public void testEntryComp1() {
	assertEquals(true, word1.compareTo(word2) == 0);
    }

    // animal is before cat, so it should be less than 0
    @Test
    public void testEntryComp2() {
	assertEquals(true, word1.compareTo(word3) < 0);
    }

    // cat is greater than animal so it should be greater than 0
    @Test
    public void testEntryComp3() {
	assertEquals(true, word4.compareTo(word1) > 0);
    }

    // equals for null should return false
    @Test
    public void testEntryEquals1() {
	assertEquals(false, word1.equals(null));
    }

    // equals for wrong type should return false
    @Test
    public void testEntryEquals2() {
	assertEquals(false, word1.equals((Integer) 69));
    }

    // equals for different words should return false
    @Test
    public void testEntryEquals3() {
	assertEquals(false, word1.equals(word3));
    }

    // equals for same word but one has a definition should return true
    @Test
    public void testEntryEquals4() {
	assertEquals(true, word1.equals(word2));
    }

    // testing basic Dictionary constructor
    @Test
    public void testDictConst1() {
	Dictionary hats = new Dictionary();
	assertEquals(0, hats.entryCount());
    }

    // testing Dictionary Constructor given a tree should return a deep copy of
    // the same tree
    @Test
    public void testDictConst2() {
	Dictionary hats = new Dictionary(tree);
	assertEquals(tree.getInorderTraversal(), hats.getSortedEntries());
    }

    // testing Dictionary constructor should return same size as given tree
    @Test
    public void testDictConst3() {
	Dictionary hats = new Dictionary(tree);
	assertEquals(7, hats.entryCount());
    }

    // testing Dictionary constructor with missing file
    @Test(expected = FileNotFoundException.class)
    public void testDictConst4() throws FileNotFoundException {
	Dictionary hats = new Dictionary("This file won't be found");
    }

    // testing Dictionary Constructor with existing file
    @Test
    public void testDictConst5() throws FileNotFoundException {
	Dictionary hats = new Dictionary("testDictionary.txt");
	assertEquals(dictFromTree.getSortedEntries(), hats.getSortedEntries());
    }

    // testing entryCount on a Dictionary constructed from a file
    @Test
    public void testDictConst6() throws FileNotFoundException {
	Dictionary hats = new Dictionary("testDictionary.txt");
	assertEquals(7, hats.entryCount());
    }

    // testing addEntry (for just a word) incrementing entryCount
    @Test
    public void testAddEntry1() {
	dictFromTree.addEntry("hello");
	assertEquals(8, dictFromTree.entryCount());
    }

    // testing addEntry (for just a word) adding at correct node
    @Test
    public void testAddEntry2() {
	dictFromTree.addEntry("hello");
	String[] temp = { "arm", "hat", "hello", "kill", "map", "orb", "rat", "zork" };
	ArrayList<Entry> expected = new ArrayList<Entry>();

	for (int i = 0; i < temp.length; i++) {
	    expected.add(new Entry(temp[i]));
	}

	assertEquals(expected, dictFromTree.getSortedEntries());
    }

    // testing addEntry (for a word with definition) incrementing entryCount
    @Test
    public void testAddEntry3() {
	dictFromTree.addEntry("hello", "a common greeting");
	assertEquals(8, dictFromTree.entryCount());
    }

    // testing addEntry (for a word with definition) adding at correct Node
    @Test
    public void testAddEntry4() {
	dictFromTree.addEntry("hello", "a common greeting");
	String[] temp = { "arm", "hat", "hello", "kill", "map", "orb", "rat", "zork" };
	ArrayList<Entry> expected = new ArrayList<Entry>();

	for (int i = 0; i < temp.length; i++) {
	    expected.add(new Entry(temp[i]));
	}

	assertEquals(expected, dictFromTree.getSortedEntries());
    }

    // test hasWord for a word (that has a definition) in the dictionary
    @Test
    public void testHasWord1() {
	assertEquals(true, dictFromTree.hasWord("map"));
    }

    // test hasWord for a word (that doesnt have a definition) in the dictionary
    @Test
    public void testHasWord2() {
	assertEquals(true, dictFromTree.hasWord("kill"));
    }

    // test hasWord again
    @Test
    public void testHasWord3() {
	assertEquals(true, dictFromTree.hasWord("zork"));
    }

    // test hasWord for a word not in the dictionary
    @Test
    public void testHasWord4() {
	assertEquals(false, dictFromTree.hasWord("scream"));
    }

    // test getDefinition of word no in the dictionary
    @Test(expected = IllegalArgumentException.class)
    public void testGetDefinition1() {
	dictFromTree.getDefinitionOf("scream");
    }

    // test getDefinition for a word in the dictionary
    @Test
    public void testGetDefinition2() {
	assertEquals("a vermin", dictFromTree.getDefinitionOf("rat"));
    }

    // test getDefinition for a word that doesn't have a definition
    @Test
    public void testGetDefinition3() {
	String res = dictFromTree.getDefinitionOf("kill");
	Boolean hats = false;
	if (res == null) {
	    hats = true;
	}

	assertEquals(true, hats);
    }

    // test removeEntry for decrementing entryCount
    @Test
    public void testRemoveEntry1() {
	dictFromTree.removeEntry("rat");
	assertEquals(6, dictFromTree.entryCount());
    }

    // test removeEntry for proper tree structure
    @Test
    public void testRemoveEntry2() {
	dictFromTree.removeEntry("rat");
	String[] temp = { "arm", "hat", "kill", "map", "orb", "zork" };
	ArrayList<Entry> expected = new ArrayList<Entry>();

	for (int i = 0; i < temp.length; i++) {
	    expected.add(new Entry(temp[i]));
	}

	assertEquals(expected, dictFromTree.getSortedEntries());
    }

    // test removeEntry for Entry not in dictionary
    @Test
    public void testRemoveEntry3() {
	assertEquals(false, dictFromTree.removeEntry("scream"));
    }

    // test removeEntry
    @Test
    public void testRemoveEntry4() {
	dictFromTree.removeEntry("scream");
	assertEquals(7, dictFromTree.entryCount());
    }

    // test update entry for a word not in dictionary
    @Test
    public void testUpdateEntry1() {
	assertEquals(false, dictFromTree.updateEntry("scream", "a loud noise"));
    }

    // test update entry for a word that already has a definition
    @Test
    public void testUpdateEntry2() {
	dictFromTree.updateEntry("hat", "something for your head");
	assertEquals("something for your head", dictFromTree.getDefinitionOf("hat"));
    }

    // test update entry for a word that doesn't have a definition
    @Test
    public void testUpdateEntry3() {
	dictFromTree.updateEntry("arm", "part of your body");
	assertEquals("part of your body", dictFromTree.getDefinitionOf("arm"));
    }

    // test printToFile to a missing file
    @Test(expected = FileNotFoundException.class)
    public void testPrintToFile1() throws FileNotFoundException {
	dictFromTree.printToFile("This file will not be found");
    }

    // test printToFile of a dictionary build from a tree
    @Test
    public void testPrintToFile2() throws FileNotFoundException {
	dictFromTree.printToFile("toPrint1.txt");

	File f = new File("toPrint1.txt");
	Scanner scan = new Scanner(f);
	ArrayList<String> result = new ArrayList<String>();
	while (scan.hasNextLine()) {
	    result.add(scan.nextLine());
	}
	scan.close();

	File f1 = new File("expected.txt");
	Scanner scan1 = new Scanner(f1);
	ArrayList<String> expected = new ArrayList<String>();
	while (scan1.hasNextLine()) {
	    expected.add(scan1.nextLine());
	}
	scan1.close();

	assertEquals(expected, result);
    }

    // test printToFile of a dictionary built from a file
    @Test
    public void testPrintToFile3() throws FileNotFoundException {
	Dictionary temp = new Dictionary("testDictionary.txt");
	temp.printToFile("toPrint1.txt");

	File f = new File("toPrint1.txt");
	Scanner scan = new Scanner(f);
	ArrayList<String> result = new ArrayList<String>();
	while (scan.hasNextLine()) {
	    result.add(scan.nextLine());
	}
	scan.close();

	File f1 = new File("expected.txt");
	Scanner scan1 = new Scanner(f1);
	ArrayList<String> expected = new ArrayList<String>();
	while (scan1.hasNextLine()) {
	    expected.add(scan1.nextLine());
	}
	scan1.close();

	assertEquals(expected, result);
    }

    // test printToFile of a dictionary built from a file, and had an add method
    // called on it
    @Test
    public void testPrintToFile4() throws FileNotFoundException {
	Dictionary temp = new Dictionary("testDictionary.txt");
	temp.addEntry("scream", "a loud noise");
	temp.printToFile("toPrint1.txt");

	File f = new File("toPrint1.txt");
	Scanner scan = new Scanner(f);
	ArrayList<String> result = new ArrayList<String>();
	while (scan.hasNextLine()) {
	    result.add(scan.nextLine());
	}
	scan.close();

	File f1 = new File("expected2.txt");
	Scanner scan1 = new Scanner(f1);
	ArrayList<String> expected = new ArrayList<String>();
	while (scan1.hasNextLine()) {
	    expected.add(scan1.nextLine());
	}
	scan1.close();

	assertEquals(expected, result);
    }
}
