package edu.iastate.cs228.hw4;

/**
 * Node class which is used by the BinarySearchTree class to store data.
 * Normally this class would be a private class inside the BST class, it is
 * public for testing purposes. Because of this, most methods and constructors
 * have been implemented. You may add new protected methods, but DO NOT modify
 * already implemented or change their behavior or you risk losing points in
 * tests.
 * 
 * @author
 */
public class Node<T> {

    /**
     * Instance variables of the Nodes which are connected to this Node and the
     * data it contains.
     */
    private Node<T> left, right, parent;
    private T data;
    private int height;

    /**
     * Creates a new Node that is disconnected from all others which stores the
     * given data.
     * 
     * @param d
     */
    public Node(T d) {
	this(null, null, null, d);
	height = 0;
    }

    /**
     * Creates a new Node that is connected to all the given Nodes and stores
     * the given data.
     * 
     * @param d
     */
    public Node(Node<T> left, Node<T> right, Node<T> parent, T d) {
	this.data = d;
	this.left = left;
	this.right = right;
	this.parent = parent;
    }

    /**
     * Gets the left child of this node
     * 
     * @return the left child of this node
     */
    public Node<T> getLeft() {
	return left;
    }

    /**
     * Sets the left child of this node
     * 
     * @param newLeft
     *            the new left child node of this node
     */
    public void setLeft(Node<T> newLeft) {
	left = newLeft;
    }

    /**
     * Gets the right child of this node
     * 
     * @return the right child of this node
     */
    public Node<T> getRight() {
	return right;
    }

    /**
     * Gets the right child of this node
     * 
     * @param newRight
     *            the new right child node of this node
     */
    public void setRight(Node<T> newRight) {
	right = newRight;
    }

    /**
     * Gets the parent of this node
     * 
     * @return the parent of this node
     */
    public Node<T> getParent() {
	return parent;
    }

    /**
     * Sets the parent for this node
     * 
     * @param newParent
     *            the parent to be set as the new parent for this node
     */
    public void setParent(Node<T> newParent) {
	parent = newParent;
    }

    /**
     * Gets the data stored in this node
     * 
     * @return the data stored in this node
     */
    public T getData() {
	return data;
    }

    /**
     * Sets the data in this node
     * 
     * @param newData
     *            the new data to be stored in this node
     */
    public void setData(T newData) {
	data = newData;
    }

    /**
     * Returns the next Node in an inorder traversal of the BST which contains
     * this Node.
     * 
     * @return the next Node in the traversal.
     */
    public Node<T> getSuccessor() {
	Node<T> current = this;
	// If the right tree is not null, then the successor lies within that
	// tree and is the lowest node
	if (current.right != null) {
	    return getMinNode(current.right);
	}
	// If the right tree is null then the successor is the top of tree where
	// the current node is the top's right
	Node<T> parent = current.parent;
	while (parent != null && current == parent.right) {
	    current = parent;
	    parent = parent.parent;
	}
	return parent;
    }

    /**
     * Returns the previous Node in an inorder traversal of the BST which
     * contains this Node.
     * 
     * @return the previous Node in the traversal.
     */
    public Node<T> getPredecessor() {
	Node<T> current = this;
	// If the left tree is not null, then the successor lies within that
	// tree and is the highest node
	if (current.left != null) {
	    return getMaxNode(current.left);
	}
	// If the left tree is null then the successor is the top of tree where
	// the current node is the top's right
	Node<T> parent = current.parent;
	while (parent != null && current == parent.left) {
	    current = parent;
	    parent = parent.parent;
	}
	return parent;
    }

    /**
     * Returns the bottom-leftmost node in the tree starting from the given
     * node.
     * 
     * @param node
     *            The node to act as the root of this algorithm
     * @return bottom-leftmost node from the given node
     */
    private Node<T> getMinNode(Node<T> node) {
	Node<T> current = node;
	while (current.left != null) {
	    current = current.left;
	}
	return current;
    }

    /**
     * Returns the bottom-rightmost node in the tree starting from the given
     * node.
     * 
     * @param node
     *            The node to act as the root of this algorithm
     * @return bottom-rightmost node from the given node
     */
    private Node<T> getMaxNode(Node<T> node) {
	Node<T> current = node;
	while (current.right != null) {
	    current = current.right;
	}
	return current;
    }
}
