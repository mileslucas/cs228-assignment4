package edu.iastate.cs228.hw4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.invoke.SwitchPoint;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.*;

import java.util.ArrayList;
import java.util.Scanner;

public class SpellChecker {

    /**
     * The regule expression for use in testing if words are valid.
     */
    private final static String REGEX = "[^a-zA-Z\\-\\']";

    /**
     * Displays usage information.
     *
     * There's no reason that you should need to modify this.
     */
    private static void doUsage() {
	System.out.println("Usage: SpellChecker [-i] <dictionary> <document>\n"
	        + "                    -d <dictionary>\n" + "                    -h");
    }

    /**
     * Displays detailed usage information and exits.
     *
     * There's no reason that you should need to modify this.
     */
    private static void doHelp() {
	doUsage();
	System.out.println("\n" + "When passed a dictionary and a document, spell check the document.  Optionally,\n"
	        + "the switch -n toggles non-interactive mode; by default, the tool operates in\n"
	        + "interactive mode.  Interactive mode will write the corrected document to disk,\n"
	        + "backing up the uncorrected document by concatenating a tilde onto its name.\n\n"
	        + "The optional -d switch with a dictionary parameter enters dictionary edit mode.\n"
	        + "Dictionary edit mode allows the user to query and update a dictionary.  Upon\n"
	        + "completion, the updated dictionary is written to disk, while the original is\n"
	        + "backed up by concatenating a tilde onto its name.\n\n"
	        + "The switch -h displays this help and exits.");
	System.exit(0);
    }

    /**
     * Runs the three modes of the SpellChecker based on the input arguments. DO
     * NOT change this method in any way other than to set the name and sect
     * variables.
     */
    public static void main(String[] args) {
	if (args.length == 0) {
	    doUsage();
	    System.exit(-1);
	}

	/*
	 * In order to be considered for the competition, set these variables.
	 */
	String name = "Miles Lucas"; // First and Last
	String sect = "B"; // "A" or "B"

	Timer timer = new Timer();

	timer.start();

	if (args[0].equals("-h"))
	    doHelp();
	else if (args[0].equals("-n"))
	    doNonInteractiveMode(args);
	else if (args[0].equals("-d"))
	    doDictionaryEditMode(args);
	else
	    doInteractiveMode(args);

	timer.stop();

	System.out.println("\nStudent name:   " + name);
	System.out.println("Student sect:   " + sect);
	System.out.println("Execution time: " + timer.runtime() + " ns");
    }

    /**
     * Carries out the Interactive mode of the Spell Checker.
     * 
     * @param args
     *            the arguments given to the main. The correct number of
     *            arguments may or may not be contained in it. Call doUsage()
     *            and exit if the parameter count is incorrect.
     */
    public static void doInteractiveMode(String[] args) {
	try {
	    Dictionary dic = new Dictionary(args[1]);
	    Files.copy(Paths.get(args[2]), Paths.get(args[2] + "~"), REPLACE_EXISTING);
	    Scanner in = new Scanner(new File(args[2] + "~"));
	    ArrayList<String> outputLines = new ArrayList<String>();
	    while (in.hasNextLine()) {
		outputLines.add(spellCheck(in.nextLine(), dic, true));
	    }
	    saveOutput(args[2], outputLines);
	    System.out.println("\nBackup created at " + args[2] + "~");
	} catch (ArrayIndexOutOfBoundsException e) {
	    System.out.println("Incorrect number of parameters");
	    doUsage();
	} catch (FileNotFoundException e) {
	    System.out.println("No such file");
	    doUsage();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    /**
     * Saves the lines of a given arrayList of strings to a file with the given
     * filename
     * 
     * @param filename
     *            path to save file at
     * @param outputLines
     *            the ArrayList of Strings to output
     * @throws FileNotFoundException
     *             throws if there is an issue creating the file with the given
     *             filename
     */
    private static void saveOutput(String filename, ArrayList<String> outputLines) throws FileNotFoundException {
	PrintWriter pw = new PrintWriter(filename);
	for (String s : outputLines) {
	    pw.println(s);
	}
	pw.close();
    }

    /**
     * Carries out the Non-Interactive mode of the Spell Checker.
     * 
     * @param args
     *            the arguments given to the main. The correct number of
     *            arguments may or may not be contained in it. Call doUsage()
     *            and exit if the parameter count is incorrect.
     */
    public static void doNonInteractiveMode(String[] args) {
	try {
	    Dictionary dic = new Dictionary(args[1]);
	    File input = new File(args[2]);
	    Scanner in = new Scanner(input);
	    while (in.hasNextLine()) {
		spellCheck(in.nextLine(), dic, false);
	    }
	} catch (ArrayIndexOutOfBoundsException e) {
	    System.out.println("Incorrect number of parameters");
	    doUsage();
	} catch (FileNotFoundException e) {
	    System.out.println("No such file");
	    doUsage();
	}

    }

    /**
     * Given a String and a Dictionary will check for spelling and output to the
     * terminal
     * 
     * @param line
     *            the String to check for spelling
     * @param dic
     *            the Dictionary to use for checking
     */
    private static String spellCheck(String line, Dictionary dic, boolean isInteractive) {
	String[] wordsInput = line.trim().split(" ");

	Word[] words = new Word[wordsInput.length];
	for (int i = 0; i < wordsInput.length; i++) {
	    words[i] = new Word(wordsInput[i]);

	}

	System.out.println(normalLine(words));
	if (findFirstMistake(words, dic) != null && !line.isEmpty()) {
	    if (isInteractive) {
		return interactiveLineEdit(dic, words);
	    } else {
		System.out.println(editorLine(dic, words));
	    }
	} else {
	    return normalLine(words);
	}
	return null;
    }

    /**
     * Interactively edits the Word array corresponding to a single line of text
     * 
     * @param dic
     *            the dictionary used for this edit
     * @param words
     *            the words in this line
     * @return Returns a String of the finished line
     */
    private static String interactiveLineEdit(Dictionary dic, Word[] words) {
	Scanner in = new Scanner(System.in);

	Word w = findFirstMistake(words, dic);
	while (w != null) {
	    System.out.println(editorLine(dic, words));
	    System.out.print(w.word + ": [r]eplace | [a]ccept? ");
	    String action = in.nextLine();
	    switch (action) {
	    case "r":
		System.out.print("Replacement text: ");
		String replacement = in.nextLine();
		w.word = replacement;
		break;
	    case "a":
		dic.addEntry(w.word);
		break;
	    default:
		break;
	    }
	    System.out.println();
	    System.out.println(normalLine(words));
	    w = findFirstMistake(words, dic);
	}
	return normalLine(words);
    }

    /**
     * Gets the first word that has a mistake in the line
     * 
     * @param words
     *            the words in the line to check
     * @param dic
     *            the dictionary to check for mistakes with
     * @return The first word with a mistake or null if none exist
     */
    private static Word findFirstMistake(Word[] words, Dictionary dic) {
	for (Word w : words) {
	    if (!w.isCorrect(dic)) {
		return w;
	    }
	}
	return null;
    }

    /**
     * Creates a String with blank space and circumflexes to mark mistakes
     * 
     * @param dic
     *            the dictionary to check for mistakes
     * @param words
     *            the words in the line
     * @return A string with circumflexes where mistakes are
     */
    private static String editorLine(Dictionary dic, Word[] words) {
	StringBuilder output = new StringBuilder();
	for (Word w : words) {
	    output.append(w.editorLine(dic) + " ");
	}
	return output.substring(0, output.length() - 1);

    }

    /**
     * Creates a normal string with the given words
     * 
     * @param words
     *            The words in the line
     * @return A string composed of the given words
     */
    private static String normalLine(Word[] words) {
	StringBuilder output = new StringBuilder();
	for (Word w : words) {
	    output.append(w.word + " ");
	}
	return output.substring(0, output.length() - 1);
    }

    /**
     * Carries out the Dictionary Edit mode of the Spell Checker.
     * 
     * @param args
     *            the arguments given to the main. The correct number of
     *            arguments may or may not be contained in it. Call doUsage()
     *            and exit if the parameter count is incorrect.
     */
    public static void doDictionaryEditMode(String[] args) {
	Dictionary dic = null;
	try {
	    dic = new Dictionary(args[1]);
	} catch (ArrayIndexOutOfBoundsException e) {
	    System.out.println("Incorrect number of parameters");
	    doUsage();
	} catch (FileNotFoundException e) {
	    System.out.println("No such file");
	    doUsage();
	}
	System.out.println("Editing " + args[1]);
	// Create backup

	try {
	    dic.printToFile(args[1] + "~");
	} catch (FileNotFoundException e) {
	    System.out.println("Backup failed and editing aborted");
	    return;
	}
	editMode(dic);
	try {
	    dic.printToFile(args[1]);
	} catch (FileNotFoundException e) {
	    System.out.println("Could not save new dictionary. No changes were saved.");
	}
	System.out.println("Wrote " + args[1] + ". Original backed up to " + args[1] + "~.");
    }

    /**
     * Runs the edit mode for the Dictionary
     * 
     * @param dic
     *            the dictionary to edit
     */
    private static void editMode(Dictionary dic) {
	Scanner in = new Scanner(System.in);
	String input;
	do {
	    System.out.print("Word: ");
	    input = in.nextLine();
	    if (!isValid(input)) {
		System.out.println("'" + input + "' is invalid. Please enter a valid word.");
		continue;
	    } else {
		if (dic.hasWord(input)) {
		    doFoundAction(input, dic);
		} else {
		    doNotFoundAction(input, dic);
		}
	    }
	} while (!input.equals("!quit"));
    }

    /**
     * The options and actions if a word is not found in edit mode in the
     * dictionary
     * 
     * @param input
     *            the input from original query
     * @param dic
     *            the dictionary being edited
     */
    private static void doNotFoundAction(String input, Dictionary dic) {
	System.out.println("'" + input + "' was not found.");
	System.out.println("[a]dd | add with [d]efinition | do [n]othing: ");
	Scanner in = new Scanner(System.in);
	String action = in.nextLine();
	switch (action) {
	case "a":
	    dic.addEntry(input);
	    break;
	case "d":
	    System.out.print("Definition: ");
	    String def = in.nextLine();
	    dic.addEntry(input, def);
	    break;
	case "n":
	    break;
	case "!quit":
	    break;
	default:
	    System.out.println("Invalid option");
	    break;
	}
    }

    /**
     * The options and actions if a word is found in edit mode in the dictionary
     * 
     * @param input
     *            the input from original query
     * @param dic
     *            the dictionary being edited
     */
    private static void doFoundAction(String input, Dictionary dic) {
	System.out.println("'" + input + "' was found.");
	System.out.println("[r]emove | [g]et definition | [c]hange definition | do [n]othing: ");
	Scanner in = new Scanner(System.in);
	String action = in.nextLine();
	switch (action) {
	case "r":
	    dic.removeEntry(input);
	    break;
	case "g":
	    String def = dic.getDefinitionOf(input);
	    if (def == null) {
		def = "<undefined>";
	    }
	    System.out.println(def);
	    break;
	case "c":
	    System.out.print("Definition: ");
	    String newDef = in.nextLine();
	    dic.updateEntry(input, newDef);
	    break;
	case "n":
	    break;
	case "!quit":
	    break;
	default:
	    System.out.println("Invalid option");
	    break;
	}
    }

    /**
     * Checks if a word is valid given a predetermined regex
     * 
     * @param input
     * @return
     */
    private static boolean isValid(String input) {
	String valid = input.replaceAll(REGEX, "");
	return valid.equals(input);
    }

    /**
     * This class creates a wrapper for a String and whether it is valid in a
     * given dictionary
     * 
     * @author Miles Lucas mdlucas@iastate.edu
     *
     */
    private static class Word {

	/**
	 * The string corresponding to this word
	 */
	private String word;

	/**
	 * Creates a new word with the given string
	 * 
	 * @param word
	 *            the string that this word wraps
	 */
	private Word(String word) {
	    this.word = word;
	}

	/**
	 * Creates a line in editor mode based off this word's word
	 * 
	 * @param dic
	 *            the dictionary to see if the word is a mistake or not
	 * @return A string in editor mode
	 */
	private String editorLine(Dictionary dic) {
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < word.length(); i++) {
		sb.append(' ');
	    }
	    if (!isCorrect(dic)) {
		sb.replace(0, 1, "^");
	    }
	    return sb.toString();
	}

	/**
	 * Given a dictionary, finds if the word is in the dictionary
	 * 
	 * @param dic
	 *            The dictionary for referenece
	 * @return True if the word exists in the given dictionary
	 */
	private boolean isCorrect(Dictionary dic) {
	    String input = word;
	    return dic.hasWord(input.replaceAll(REGEX, ""));
	}
    }

    /**
     * Timer class used for this project's competition. DO NOT modify this class
     * in any way or you will be ineligible for Eternal Glory.
     */
    private static class Timer {
	private long startTime;
	private long endTime;

	public void start() {
	    startTime = System.nanoTime();
	}

	public void stop() {
	    endTime = System.nanoTime();
	}

	public long runtime() {
	    return endTime - startTime;
	}
    }
}
